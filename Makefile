
LATEX=pdflatex
LATEX_FLAGS=-file-line-error -halt-on-error -interaction nonstopmode

BIBTEX=bibtex



BASEFILE=notes.tex
AUXFILE=build/notes.aux
OUTDIR=build
OUTFILTER=./filter.py

default: setup
	@$(LATEX) $(LATEX_FLAGS) -output-directory $(OUTDIR) $(BASEFILE) 2>&1 | $(OUTFILTER)
	@$(LATEX) $(LATEX_FLAGS) -output-directory $(OUTDIR) $(BASEFILE) > /dev/null 
	@$(LATEX) $(LATEX_FLAGS) -output-directory $(OUTDIR) $(BASEFILE) > /dev/null 

show:
	$(LATEX) $(LATEX_FLAGS) -output-directory $(OUTDIR) $(BASEFILE)

spellcheck:
	@echo "To use this you need to run:"
	@echo "\tsudo aptitude install ispell ibritish"
	@echo ""
	@echo "Have you committed? ispell will mutate the files (press ctrl+c to cancel) "
	@read x
	aspell check --mode=tex --dont-backup --sug-mode=fast --master=en_GB $(BASEFILE)

clean:
	rm -f $(OUTDIR)/*
	rmdir $(OUTDIR)

setup: $(OUTDIR)

$(OUTDIR): 
	mkdir -p $(OUTDIR)
