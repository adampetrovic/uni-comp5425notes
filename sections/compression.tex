
\subsection{Compression Basics}
    \textbf{Why Compression?}
    \newline
    \begin{itemize}
        \item Increasing multimedia data
        \item Multimedia data is large in size: demands storage and bandwidth
        \item Represent the data with the smallest possible bits
    \end{itemize}

    \textbf{How to Compress?}
    \newline
    \begin{itemize}
        \item Remove redundancy
        \begin{itemize}
            \item Data representation redundancy
            \begin{itemize}
                \item Coding redundancy: representing information with too many bits
                \item Spatial/temporal redundancy: Dependence between neighbour samples
                \item Spectural redundancy
            \end{itemize}
            \item This usually leads to lossless compression
        \end{itemize}
        \item Perception redundancy
        \begin{itemize}
            \item Human beings can tolerate some information distorting or loss without affecting the effectiveness of communication
            \item This usually leads to lossy compression
        \end{itemize}
        \item Lossless vs Lossy compression
    \end{itemize}
    

\subsection{Audio Compression}
    \subsubsection{Audio Background}
        \begin{itemize}
            \item Audio is caused by a disturbance in air pressure that reaches the human eardrum
            \item Audio is characterised by frequency and amplitude
            \item Frequency of audible sound ranges from 20 to 20kHz. The higher the requency the more shrilly; the lower frequency the more toneless.
            \item The amplitude is usually measured with decibel (dB)
        \end{itemize}
        \begin{equation*}
            dB = 20\log_{10}(\frac{X}{Y})
        \end{equation*}

    \subsubsection{Digitization}
        \begin{center}
            \includegraphics[width=0.5\textwidth]{./aux_files/compression/digitization_audio.png}
        \end{center}
        \begin{itemize}
            \item Quantization Level
            \begin{itemize}
                \item The number of quantization levels used determines the amplitude fidelity of the digital 
                signal relative to the original analog signal.
                \item The large the number of quantization levels, the smaller the quantization step,
                the smaller the quantization noise, and the higher fidelity can be achieved.
            \end{itemize}
            \begin{equation*}
                b = log_2(Q)
            \end{equation*}
            b is the number of bits needed, Q is the number of quantization levels
            \newline
            \textbf{Reference to more of this in Week 8 page 7/37} 
        \end{itemize}

    \subsubsection{PCM, DPCM \& ADPCM}
        \begin{itemize}
            \item \textbf{PCM Coding}
            \begin{itemize}
                \item The combination of the steps sampling, quantization, and binary coding.
                \item As a general rule, the highest possible sampling rate and sample size should be used,
                minimize deterioration of the signal when it is processed.
                \item If a compromise must be made, the effect on quality of reducing the sampling size is
                more drastic than that of reduced sampling rate - the same reduction in size can be produced
                by halving the sample rate of halving the sample size; the former is better.
                \item A smaller sampling size reduces the amount of data, but because there are fewer values 
                you don't get as much accuracy. This loss of accuracy is more of a problem for quiet
                sounds than for loud ones.
                \begin{itemize}
                    \item If your range of values is -64 to +63, then any sound less than 1/128 of the loudest
                    possible sound will disappear.
                \end{itemize}
            \end{itemize}
            \item \textbf{Nonlinear PCM Coding}
            \begin{itemize}
                \item Human hearing is relatively insensitive to small error in loud sounds but more sensitive
                to similar errors in quiet sounds. Linear PCM coding, however, allows the same degree of error
                regardless of the amplitude.
                \item The nonlinear PCM coding works well for compression. It consists of using the 
                available bits more efficiently; you use more bits for quiet sounds in which the data loss
                is more audible.
                \item One way to address the loss of quieter sounds is to redefine your numbers.
                \item Basic PCM coding is linear, which means, for example that a value of 32 represents exactly
                twice as much sound intensity as a value of 16; a value of 63 represents exactly 63 times as
                much sound intensity as a value 1.
                \item To get a wilder dynamic range, you can use nonlinear encodings, in which a value of 
                1 might represent much less than 1/63 of the intensity represent by 63.
                \item The two most common nonlinear PCM coding methods are mew-Law and A-Law compression
                which were later incorporated into the ITU G.711 standard. They all use logarithmic formulas
                to convert 16- or 12-bit linear PCM samples into 8-bit codes
                \item These approaches are also called Logarithmic Compression. The work well for several reasons:
                \begin{itemize}
                    \item Most sound consists predominiantly of small samples. These encodings provide more
                    accuracy for these common values at the cost of less accuracy for relatively infrequent
                    large samples;
                    \item Human hearing is logarithmic; a change in the intensity of a quiet sound is more 
                    noticeable than a similar change in the intensity of a loud sound.
                \end{itemize}
                \item Essentially, logarithmic encoding provides more accuracy where that accuracy is most
                audible.
                \item \textbf{Lookup mew-Law Compression in Week 8 page 8-9/37}
            \end{itemize}
            \item \textbf{Linear vs. Non-linear}
            \begin{itemize}
                \item Linear PCM will always be preferred for manipulating sound. Whenever you mix two sounds
                by adding or adjust volume by multiplication, you are assuming your sound format is linear.
                \item Nonlinear PCM coding is more suitable for transferring and storing sound.
            \end{itemize}
            \item \textbf{Predicitive Coding}
            \begin{itemize}
                \item The idea behind many compression techniques is to find a way to predict the next data 
                element by looking at data that has already appeared. Intuitively, if you can correctly
                guess the next element, you do not need to store that element, because the decompressor can
                use the same technique to guess it correctly. This idea is also referred to as data modeling.
                \item You can build a simple compression engine from any predictor function
                \begin{itemize}
                    \item E.g. have the compressor compare the predicted value with the actual value.
                    If the prediction is right, the compressor outputs a one bit. Otherwise, it outputs a 
                    zero bit followed by the actual value. If the predictor is exactly correct much of the time,
                    the output of this process with be smaller than the input.
                \end{itemize}
                \item \textbf{This continues on page 10/37 Week 8}
            \end{itemize}
            \item \textbf{Subhand Coding}
            \begin{itemize}
                \item One factor that limits the effectiveness of differential encoding techniques is frequency
                \item Low-frequency sounds tend to produce lots of small differences while higher-frequency sounds
                tend to produce larger differences. One way to improve differential techniques is to divide
                the sound into two or more frequency ranges, or sub-bands, and compress each one separately.
            \end{itemize}
            \item \textbf{Perceptually-based Compression} 
            \begin{itemize}
                \item If an audio signal is digitized in a straightforward way, data corresponding to sounds
                that are inaudible may be included in the digitized version.
                \item This is because the signal records all the physical variations in air pressure that
                cause sound, but the perception of sound is a sensation produced in the brain, via the ear, but
                the ear and brain do not respond to the sound waves in a simple way.
                \item Two phenomena in particular cause some sounds not to be heard, despite being physically
                present. Both are familiar experiences: a sound may be too quiet to be heard, or it may be 
                obsured by some other sound.
                \item \textbf{continues on page 10/37 of Week 8}  
            \end{itemize}
        \end{itemize}
        \begin{center}
            \includegraphics[width=0.8\textwidth]{./aux_files/compression/perceptually_based_audio_codec.png}
        \end{center}
    \subsubsection{MPEG 1/2/4}
    \begin{itemize}
        \item The Moving Pictures Expert Group (MPEG) first met in May of 1998.
        \item MPEG standards are best known for their video compression, however they also support high-quality
        audio compression. MPEG audio has been so successful that is often used on its own purely for compressing
        sound, esspecially music.
        \item It allows three sampling rates: 32, 44.1, 48kHz
        \item The committee conducted extensive subjective listening tests during the development of the standard
        \item It's a family of three audio compression schemas which are called MPEG-Audio layer 1, 2 and 3.
        \item The compression standard defines a family of algorithms appropriate for a wide range of audio material
        (e.g. speech, music, and the range of spatial effects that might be expected on a movie soundtrack)
        based on a combination of subhand coding and perceptually based compression with techniques to explicitly
        exploit properties of human hearing.
    \end{itemize}
        \begin{center}
            \includegraphics[width=\textwidth]{./aux_files/compression/MPEG.png}
        \end{center}
    \begin{itemize}
        \item \textbf{MPEG-1 Audio}
        \begin{itemize}
            \item MPEG-1 is the first of the standards.
            \item Contains 5 parts (systems; video; audio; conformance; software) in which Part 3 (audio)
            relates how to compress one- or two-channel audio, giving near CD quality at a bit rate of
            256 kbits/s.
            \item The audio portion of MPEG-1 is itself divided into three layers. Each layer provides
            successively better quality at the cost of a more complex implementation. Layer 1 is the simplest.
            \begin{itemize}
                \item Best suited to situations where data can be transferred quickly (e.g. from a HDD)
                \item However, computation speed is limited.
                \item Layer 3 offers the best quality when data size is critical, but it requires more
                computational power to compress and decompress.
            \end{itemize}
            \item \textbf{Layers}
                \begin{itemize}
                    \item MPEG audio standard addresses different compliance points so as to be cost-effective
                    for a variety of applications. The compliance points in MPEG audio are called layers,
                    ordered from 1 to 3 in increasing complextiy but better quality, and decreasing the data 
                    rate of the compressed audio.
                    \item An implementation of a higher layer must be able to decode the MPEG audio signals of
                    lower layers.
                    \item In principle, the encoder chooses the layers, depending on how much quality is needed
                    or equivalently how much compression is desirec.
                    \item Three layers are in no way hierarchical, but three different coding algorithms.
                    \item Layer 1
                    \begin{itemize}
                        \item Frequency masking: usage of a DCT-based filter. At any given time the algorithm
                        only consider one frame. The frequency occuring in this frame are subdivided into the
                        frequency bands and then filtered.
                    \end{itemize}
                    \item Layer 2
                    \begin{itemize}
                        \item Temporal masking: at any given time the algorithm looks at three adjacent frames,
                        the current, the previous and the next frame. This allows to take advantage of temporal
                        masking effects as perceived by the human ear.
                    \end{itemize}
                    \item Layer 3
                    \begin{itemize}
                        \item Non-linear masking: the frequencies are subdivided into bands of different width.
                        Also stereo channels are encoded differentially, i.e. the difference between the two
                        channels rather than the absolute values are encoded.
                    \end{itemize}
                \end{itemize}
                \item \textbf{MPEG-1 Audio: MP3}
                \begin{itemize}
                    \item MPEG-1 Layer 3 audio: commonly known as MP3, can achieve compression ratios up to
                    14:1, while maintaining high/near-CD quality.
                    \item Using MP3 format, a typical track from a CD can be compressed to under 3Mbytes and
                    a typical 600-Mbyte audio CD can be turned into a 50-Mbyte MP3 collection. Therefore,
                    the audio files with MP3 format are small enough to be distributed via Internet.
                    \item Through the process of "streaming", users can download MP3 files in segments and
                    listen to them as they are downloaded. This is the most common file format and data
                    reduction scheme for delivering audio, particularly music, on the Web.
                \end{itemize}
                
        \end{itemize}
        
    \end{itemize}

\subsection{Video Compression}

    \subsubsection{Digitization}
        The representation of a spatio-temporally sampled video scene in digital form.
        \begin{itemize}
            \item A real visual scene is continuous both spatially and temporally.
            \begin{itemize}
                \item In order to represent and process a visual scene digitally, it is necessary to sample
                the real scene spatially (typically on a rectangular grid in the video image plane) and
                temporally (typically as a series of "still" images or frames sampled at regular intervals
                in time).
            \end{itemize}
        \end{itemize}
        \begin{center}
            \includegraphics[width=0.5\textwidth]{./aux_files/compression/video_digitization.png}
        \end{center}
        \begin{itemize}
            \item Video Color Systems
            \begin{itemize}
                \item Televisions also display video using the RGB system based upon three types of phosphor
                that emit light in the reg, green and blue regions of the spectrum.
                \item Unlike computers, television/video signals are not transmitted or stored in RGB
                \begin{itemize}
                    \item In Black-White TV the only piece of information being sent was the brightness known
                    as the luminance for each dot on the TV screen.
                    \item  This produced the shades of grey
                    \item When color television was being developed, it was imperative that color broadcasts could
                    be viewed on black and white televisions, so that millions of people didn't have to throw
                    out the sets they already owned.
                    \item The broadcast sends Y.C.C. scheme instead of RGB to accommodate the gradual 'roll-over'
                    color TV.
                \end{itemize}
                \item In Y.C.C the Y was the same old luminance (brightness) signal while the Cs stood for the
                color signal.
                \item The two color components would determine the hue of a pixel
                \item Video Color Systems
                \begin{itemize}
                    \item \textbf{YIQ}
                    \item \textbf{YUV}
                    \item \textbf{YDrDb} 
                \end{itemize}
                \item Spatial Sampling
                \begin{itemize}
                    \item Spatial sampling consists of taking measurements of the underlying analog
                    signal at a finite set of sampling points in a finite viewing area (or frame)
                    \item To simplify the process, the sampling points are restricted to lie on a lattice,
                    usually a rectangular grid.
                \end{itemize}
                \begin{center}
                    \includegraphics[width=0.8\textwidth]{./aux_files/compression/samplingpoints.png}
                \end{center}
            \end{itemize}
            \item Video Resolution
            \begin{itemize}
                \item The visual quality of the video image is influenced by the number of sampling points.
                \item More sampling points (a higher sampling resolution) give a finer representation of the
                image
                \item Though, more sampling points require higher storage capacity!
            \end{itemize}
            \item Temporal Sampling
            \begin{itemize}
                \item A moving video image is formed by sampling the video signals temporally, taking a
                rectangular snapshot of the signal at a periodic time interval.
                \item The human eye is relatively slow in responding to temporal changes. By showing at least
                16 frames of a video per second, an illution of motion is created. This observation is the basis
                for motionpicture technology, which typically performs temporal sampling at a rate of 24 frames.
            \end{itemize}
            \item Digital Video Standards
            \begin{itemize}
                \item To promote the interchange of digital video data, several formats for representing
                video data have been standardized.
                \item Information on video standards \textbf{Week 08 page 29} 
            \end{itemize}
            \item Digital Video Storage
            \begin{itemize}
                \item For a two-hour movie encoded in the NTSC CCIR-601 4:2:2 format the uncompressed video
                representation would require about 151 gigabytes to store.
                \item Compression algorithm needed
            \end{itemize}
        \end{itemize}
    \subsubsection{Video Data Redundancy}
        \begin{itemize}
            \item Redundancy exists in a video sequence in two forms: spatial and temporal.
            \item Spatial is also called intra-frame redundancy - refers to the redundancy within a single
            frame of video, while the latter, also called inter-frame redundancy refers to the redundancy
            that exists between consecutive frames within a video sequence.
            \item Spatial/Intra-frame compression
            \begin{itemize}
                \item Reducing spatial redundancy has been the focus of many digital image compression
                \item Since video is just a sequence of images, image compression techniques are directly
                applicable to video frames.
                \item Some popular image lossless/lossy coding techniques could be useful (see image compression
                above)
            \end{itemize}
            \item Temporal/Inter-frame Compression
            \begin{itemize}
                \item Successive frames in a video sequence are typically highly correlated, especially for 
                scenes where there is little or no motion.
                \item Inter-frame compression methods exploit the temporal redundancies due to similarity
                between neighbouring frames, in addition to the spatial, spectral and psycho-visual redundancies
                to provide superior compression efficiency.
                \item Basic techniques for reducing temporal redundancy:
                \begin{itemize}
                    \item Subsampling
                    \item Frame difference: a very simple technique for exploiting temporal redundancy in a video
                    sequence is to code the difference between one frame and the next. This is the frame 
                    difference and is an extension of the busic differential pulso code modulation (DPCM) coding
                    techniques. (for more see Week 08 page 32)
                    \item Motion compensation: frame differencing can be viewed as a predictive coding technique
                    where the prediction is simply the previous decoded frame. By improving the prediction, we
                    can potentially obtain better compression. Motion compensation is one such technique that
                    uses a model of the motion of objects between frames to form a prediction. Using the motion
                    model, the encoder performs Motion Estimation (ME) to determine the motion that exists
                    between a reference frame and the current frame. \textbf{More on this: week 08 page 33} 
                \end{itemize}
            \end{itemize}
        \end{itemize}
\subsection{Image Compression}

    \subsubsection{JBIG}
        \begin{itemize}
            \item Joint Bi-Level image experts group (JBIG) is a group of experts for bi-level (black/white)
            image coding. Its official name is "ITU-T recommendation T.82"
            \item There are plenty of black and white images, especially for documents, which should be
            handled particularly to achieve better compression performance.
            \item The operation of the JBIG encoder is not degined in detail. The JBIG dicusses the details
            of the decoder and the format of the compressed file.
            \item JBIG can be viewed as a combination of two algorithms, a lossless compression algorithm
            and a progressive transmission algorithm.
            \item JBIG is based on multiple arithmetic coding, that is different arithmetic coders are applied
            to different data
            \begin{itemize}
                \item arithmetic coding encodes variable length of symbols into variable length of codes.
            \end{itemize}
            \item \textbf{More on JBIG in Week 08 page 14}
        \end{itemize}
    \subsubsection{DCT}
        \begin{itemize}
            \item DCT is to make the transform easier by considering the cosine part only.
            \item Used within JPEG compression
        \end{itemize}

    \subsection{Types of Compression for JPEG}
        \begin{itemize}
            \item Lossy Compression
            \begin{itemize}
                \item Shift the pixel value to reduce precision requirement by subtracting 128
                \item Convert color images into YCbCr components
                \item Downsample CbCr components
                \item DCT each pixel block
                \item Quantize DCT coefficients and eliminate near-zero ones
                \item Unwrap the coefficients
                \item Lossless coding (huffman, RLE or Arithmetic coding)
                \item Add header info and output the result
                \item More on this Week 08 Page 17-20
            \end{itemize}
            \item Lossless JPEG compression
            \begin{itemize}
                \item This coding scheme is rarely used in practice
                \item The basic idea is that the value of the current pixel is predicted from the values of the
                previous pixels. The difference between the predicted value and the actual value is what is 
                coded using Huffman or arithmetic methods.
                \item Details on this in Week 08 pages 22-23.
            \end{itemize}
            \item Wavelet Compression
            \begin{itemize}
                \item Like the transform coding technique, wavelet coding is based on the idea that the
                coefficients of a transform that de-correlates the pixels of an image can be coded more 
                eficiently than the original pixels themselves.
                \item If the wavelet kernel pack most of the important visual information into a smaller
                number of coefficients, the remaining coefficients can be quantized coarsely or truncated to 
                zero with little image distortion.
                \item \textbf{More in this in Week 08 page 23-24} 
            \end{itemize}
        \end{itemize}

    \subsection{JPEG}
        \begin{itemize}
            \item Joit Photographic Experts Group (JPEG)
            \item JPEG coding standard (1998): DCT (discrete cosine transform) based transform coding to 
            compress bit-map images.
            \item JPEG is designed for efficiently compressing either true-color (24bit) or grey-level images of 
            natural, real-world scenes, and is now the de factor standard for DSC (digital still camera) systems
            \item Works well on photographs, naturalistic artwork, and similar material; not so well on simple 
            cartoons, lettering, line drawings or one colored blocks.
            \item JPEG is designed to exploit known limitations of the human eye, notably the fact that small
            color changes are perceived less accurately than small changes in brightness. Thus JPEG is intended 
            for compressing images that will be looked at by humans.
            \item \textbf{More on JPEG in week 08 page 15}
            \item \textbf{JPEG2000 Objectives} 
            \begin{itemize}
                \item Low bit-rate compression performance
                \item Lossless and lossy compression
                \item LArge images (greater than 64K by 64K without tiling)
                \item Single decompression architecture
                \item Transmission in noisy environments (beyond restart intervals)
                \item Computer generated imagery
                \item Compound document (text, natural image and graphics mixed)
                \item Random code stream access and processing
                \item Open artchitecture
                \item Progressive transmission by pixel accuracy and resolution
            \end{itemize}
            \item \textbf{See more in Week 08 page 27/27} 
        \end{itemize}
    \subsection{Spatial Coding and Spectral Coding}
    \begin{itemize}
        \item Spatial coding is probably the most well known and the most intuitive coding method
        \begin{itemize}
            \item The signal is recorded in terms of the amplitudes of the sampling points. For example, PCM
            coding for audio waveform.
        \end{itemize}
        \item Spectral coding is to exploit the signal properties in the frequency domain.
    \end{itemize}
    \begin{equation*}
         For example, F(x) = G(f(x)), f(x)=G^{-1}(F(x))
    \end{equation*}
    \textbf{More on Spectral Coding in Week 08 page 16} 

    \subsection{Fourier Transform}
        \begin{itemize}
            \item Fourier transform is to convert a signal from spatial fomain to frequency domain.
            \item The inverse Fourier transform is to convert a spectral signal into spatial signal.
        \end{itemize}
        \textbf{More on this in Week 08 page 16/37} 
        
